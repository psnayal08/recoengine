
from collections import Counter
from service.aerospike_db_manager import AeroSpikeDbManager
from schema.article import Article
from operator import itemgetter
from constant.constants import ARTICLE_SET
from scipy.spatial.distance import cosine, euclidean
from pandas.core.frame import DataFrame
from util.rake_helper import RakeHelper

class ArticleSimilarity:
    __areospike_db_manager = AeroSpikeDbManager()
    
    def get_similar_articles(self, article_id, title, categories =[], tags = [] ):
        '''
         fetchs most similar articles for given article and 
         if similar articles are not  present then calls for batch calculator to calculate similar articles
         
        :param article_id: id of article for which similar articles is to be fetched.
        :param categories: categories of article for which similar articles is to be fetched.
        :param tags: tags present in article
        :return list of recommended article ids or empty list if nothing could be found
        '''
        article = self.__areospike_db_manager.get_record_by_primary_key(ARTICLE_SET, article_id)
        if article is not None:
            if Article.SIMILAR_ARTICLES not in article or len(article[Article.SIMILAR_ARTICLES] ) == 0:
                self.batch_calculator_for_similar_articles_by_both_cosine_similarity_and_eclidean_distance(article_id, article[Article.TITLE], article[Article.CATEGORIES],  article[Article.TAGS],  False)
                article = self.__areospike_db_manager.get_record_by_primary_key(ARTICLE_SET, article_id)
            return article[Article.SIMILAR_ARTICLES]
        else:
            self.batch_calculator_for_new_pushed_article(article_id, title, categories, tags)
    
    def batch_calculator_for_new_pushed_article(self,article_id, title, categories = [], tags = []):
        '''
        Runs a batch calculator whenever a new article is pushed to db 
        for determining most similar articles to given article
         
        :param article_id: Id of article for which similar articles are to be calculated  .
        :param categories: list of categories present in article.
        :param tags: list of tags present in articles.
        :param is_new_article_pushed: whether article is first time pushed to db (True or False).    
        '''
        
        record = {Article.ID:article_id, Article.TITLE : title, Article.CATEGORIES:categories, Article.TAGS:tags}
        self.__areospike_db_manager.save_record(ARTICLE_SET, article_id, record)
        self.batch_calculator_for_similar_articles_by_both_cosine_similarity_and_eclidean_distance(article_id, title, categories, tags, True)
        return self.__areospike_db_manager.get_record_by_primary_key(ARTICLE_SET, article_id)
        
    def batch_calculator_for_similar_article_by_cosine_similarity(self, article_id, categories = [],tags = [],is_new_article_pushed = False):
        '''
        Using cosine similarity calculates most similar articles for 
        given article and save into db.
         
        :param article_id: Id of article for which similar articles are to be calculated  .
        :param categories: list of categories present in article.
        :param tags: list of tags present in articles.
        :param is_new_article_pushed: whether article is first time pushed to db (True or False).    
        '''
        
        all_articles  = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        recommend_articles = []
        if all_articles and len(all_articles) > 1:
            for article in all_articles:
                if Article.ID in article and  article[Article.ID] != article_id:
                    #if article has any common category with  comparing article calculate some weightage  
                    article_similar_category_weightage =  self.get_similer_category_weightage(categories, article[Article.CATEGORIES])
                    
                    #for calculating cosine similarity weightage invert the cosine similarity between two articles
                    #(the less distance  between them  more weightage is given)
                    cosine_similarty_weightage = str(-(self.calculate_cosine_similarity_between_two_articles_by_tags(tags, article["tags"])))
                    
                    #change weightage  value  (can be in exponential number) to float value 
                    cosine_similarty_weightage = self.__normlize_weightage(cosine_similarty_weightage)
                    article_similarity_weightage = article_similar_category_weightage + cosine_similarty_weightage
                    recommend_articles.append({Article.WEIGHTAGE :article_similarity_weightage ,Article.ID:article[Article.ID]})
                    self.__post_knn_calculation(recommend_articles, article_id, is_new_article_pushed)
        else:
            record = {Article.SIMILAR_ARTICLES:recommend_articles}
            self.__areospike_db_manager.save_record(ARTICLE_SET, article_id, record)
        
    def batch_calculator_for_similar_articles_by_euclidean_distance(self, article_id, title, categories = [],is_new_article_pushed = False):
        '''
        Using euclidean distance calculates most similar articles for 
        given article and save into db.
        Uses rake to extract keywords from article title and
        on the basis of those keywords calculate euclidean distance.
         
        :param article_id: Id of article for which similar articles are to be calculated  .
        :param categories: list of categories present in article.
        :param title: title of article.
        :param is_new_article_pushed: whether article is first time pushed to db (True or False).    
        '''
        all_articles  = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        article_keywords = RakeHelper.get_list_of_keywords_using_rake(self, title)
        recommend_articles = []
        if all_articles and len(all_articles) > 1:
            for article in all_articles:
                if Article.ID in article and  article[Article.ID] != article_id:
                    
                    #if article has any common category with  comparing article calculate some weightage  
                    article_similar_category_weightage =  self.get_similer_category_weightage(categories, article[Article.CATEGORIES])
                    similar_article_keywords = RakeHelper.get_list_of_keywords_using_rake(self,article[Article.TITLE])
                    
                    #for calculating euclidean distance weightage invert the euclidean distance between two articles
                    #(the less distance  between them  more weightage is given)
                    e_distance_weightage = str(-(self.calculate_euclidean_distance_between_two_articles_by_keywords(article_keywords, similar_article_keywords)))
                   
                    #change weightage  value  (can be in exponential number) to float value 
                    e_distance_weightage = self.__normlize_weightage(e_distance_weightage)
                    article_similarity_weightage = article_similar_category_weightage + e_distance_weightage
                    recommend_articles.append({Article.WEIGHTAGE :article_similarity_weightage ,Article.ID:article[Article.ID]})
                    self.__post_knn_calculation(recommend_articles, article_id, is_new_article_pushed)
        else:
            record = {Article.SIMILAR_ARTICLES:recommend_articles}
            self.__areospike_db_manager.save_record(set, article_id, record)
            

    def batch_calculator_for_similar_articles_by_both_cosine_similarity_and_eclidean_distance(self, article_id, title, categories = [], tags = [], is_new_article_pushed = False):
        '''
        Using euclidean distance and cosine similarity both calculates most similar articles for 
         
        :param article_id: Id of article for which similar articles are to be calculated  .
        :param categories: list of categories present in article.
        :param tags: list of tags present in article.
        :param title: title of article.
        :param is_new_article_pushed: whether article is first time pushed to db (True or False).    
        '''
        all_articles  = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        article_keywords = RakeHelper.get_list_of_keywords_using_rake(self, title)
        recommend_articles = []
        if all_articles and len(all_articles) > 1:
            for article in all_articles:
                if Article.ID in article and  article[Article.ID] != article_id:
                    article_similar_category_weightage =  self.get_similer_category_weightage(categories, article[Article.CATEGORIES])
                    similar_article_keywords = RakeHelper.get_list_of_keywords_using_rake(self,article[Article.TITLE])
                    e_distance_weightage = str(-(self.calculate_euclidean_distance_between_two_articles_by_keywords(article_keywords, similar_article_keywords)))
                    e_distance_weightage = self.__normlize_weightage(e_distance_weightage)
                    cosine_similarty_weightage = str(-(self.calculate_cosine_similarity_between_two_articles_by_tags(tags, article[Article.TAGS])))
                    cosine_similarty_weightage = self.__normlize_weightage(cosine_similarty_weightage)
                    article_similarity_weightage = article_similar_category_weightage + e_distance_weightage + cosine_similarty_weightage
                    recommend_articles.append({Article.WEIGHTAGE :article_similarity_weightage ,Article.ID:article[Article.ID]})
                    self.__post_knn_calculation(recommend_articles, article_id, is_new_article_pushed)
        else:
            record = {Article.SIMILAR_ARTICLES:recommend_articles}
            self.__areospike_db_manager.save_record(set, article_id, record)
    
    
          
    def get_similer_category_weightage(self, categories_first = [], categories_second = [] ):
        '''
        Checks for similar category in both lists and 
        returns weightage accordingly  
         
        :param categories_first: first list of categories .
        :param categories_second: second list of categories  
        :return similar_category_weightage
        '''
        similar_category_weightage = 0.0
        if any(map(lambda v: v in categories_first, categories_second)):
            similar_category_weightage = 0.1
            return similar_category_weightage
        return similar_category_weightage
    
        
        
    def calculate_cosine_similarity_between_two_articles_by_tags(self,article_first_tags,article_second_tags ):
        '''
        Calculates cosine similarity between two articles 
        on the basis of their tags 
         
        :param article_first_tags: first list of tags .
        :param article_second_tags: second list of tags  
        :return cosine similarity 
        '''
        
        article_first_tags_word_count = Counter(article_first_tags)
        article_second_tags_word_count = Counter( article_second_tags)
        tags  = list(article_first_tags_word_count.keys() + article_second_tags_word_count.keys())

        #get vectors of tags  to calculate cosine similarity
        article_first_tags_vect = [article_first_tags_word_count.get(tag, 0) for tag in tags]
        article_second_tags_vect = [article_second_tags_word_count.get(tag, 0) for tag in tags]

        df = DataFrame({"article_first_tags": article_first_tags_vect,
                 "article_second_tags": article_second_tags_vect
                 })
        return cosine(df["article_first_tags"], df["article_second_tags"])
            
    
        
    def calculate_euclidean_distance_between_two_articles_by_keywords(self,article_first_keywords,article_second_keywords ):
        '''
        Calculates euclidean distance between two articles 
        on the basis of given keywords 
         
        :param article_first_keywords: first list of tags .
        :param article_second_keywords: second list of tags  
        :return euclidean distance  
        '''  
        
        article_first_kewords_word_count = Counter(article_first_keywords)
        article_second_kewords_word_count = Counter(article_second_keywords)
        kewords  = list(article_first_kewords_word_count.keys() + article_second_kewords_word_count.keys())

        #get vectors of keywords  to calculate euclidean distance
        article_first_kewords_vect = [article_first_kewords_word_count.get(keword, 0) for keword in kewords]
        article_second_kewords_vect = [article_second_kewords_word_count.get(keword, 0) for keword in kewords]

        df = DataFrame({"article_first_kewords": article_first_kewords_vect,
                 "article_second_kewords": article_second_kewords_vect
                 })
        return euclidean(df["article_first_kewords"], df["article_second_kewords"])
    
        
    def __set_similar_articles_according_to_new_pushed_article(self,records):
        '''
        
        For every similar article of new pushed  article ,
        recalculate similar articles for them 
         
        :param records: list of recommended articles  .
        ''' 

        
        for record in records:
            article = self.__areospike_db_manager.get_record_by_primary_key(ARTICLE_SET, record[Article.ID])
            if Article.SIMILAR_ARTICLES not in article or len(article[Article.SIMILAR_ARTICLES] ) == 0:
                self.get_similar_articles(article[Article.ID], article[Article.TITLE],  article[Article.CATEGORIES], article[Article.TAGS])
            else:
                article[Article.SIMILAR_ARTICLES].append({Article.WEIGHTAGE:record[Article.WEIGHTAGE],Article.ID:record[Article.ID]})
                sorted_recommended_article_list = sorted(article[Article.SIMILAR_ARTICLES], key=itemgetter(Article.WEIGHTAGE),reverse = True)
                new_record = {Article.SIMILAR_ARTICLES:sorted_recommended_article_list[0:6]}
                self.__areospike_db_manager.save_record(ARTICLE_SET, article[Article.ID], new_record)
                

        
    def __post_knn_calculation(self, recommend_articles, article_id,  is_new_article_pushed):
        
        '''
        Sort the given articles according to weightge key in desc order
        and save into db.
         
        :param recommend_articles: list of  recommended article ids for given article .
        :param article_id: Id if article 
        :param is_new_article_pushed: whether article is first time pushed to db (True or False). 
        '''  
        sorted_recommended_article_list = sorted(recommend_articles, key=itemgetter(Article.WEIGHTAGE),reverse = True)
        six_sorted_recommendec_articles = sorted_recommended_article_list[0:6]
        record = {Article.SIMILAR_ARTICLES:six_sorted_recommendec_articles}
        self.__areospike_db_manager.save_record(ARTICLE_SET, article_id, record)
        if not is_new_article_pushed:
            return
        self.__set_similar_articles_according_to_new_pushed_article(six_sorted_recommendec_articles)
    
    
    
    def __normlize_weightage(self, weightage):
        if 'e'in weightage or 'E'in weightage:
            weightage = float(weightage.split('e')[0])
        else:
            weightage = float(weightage)
        return weightage
    
    

        

            


        
        
        
