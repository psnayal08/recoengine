# -*- coding: utf-8 -*-
from config import client
from constant.constants import NAMESPACE
from aerospike import predicates as p
from aerospike.exception import AerospikeError
from util.logger import custom_logger
from util.rest_response import RestResponse
import httplib


class AeroSpikeDbManager:
    
    def save_record(self, set, primary_key, record = {}):
        '''
         saves record to given set in db
         
        :param set: Name of the set where record is to be saved.
        :param primary_key: primary key of set.
        :param record: A dictionary containing key value pairs of record
        :return record saved in database if success  else aerospike exception
        '''
        try:
            key = (NAMESPACE, set , primary_key)
            client.put(key, record)
            (key, meta, record) = client.get(key)
            return record
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
            
    
    def get_record_by_primary_key(self,set,primary_key):
        '''
        Fetches record from db on the basis of given set
         and primary key
         
        :param set: Name of the set from which record is to be fetched.
        :param primary_key: primary key of set.
        :return record  if success  else aerospike exception
        '''
        try:
            key = (NAMESPACE, set , primary_key)
            (key, meta, record) = client.get(key)
            return record
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
    
    def delete_key(self,set,primary_key):
        '''
        Removes key from set 
         
        :param set: Name of the set from which key is to be deleted.
        :param primary_key: primary key of set.
        :return record 
        '''
        try:
            key = (NAMESPACE, set , primary_key)
            client.remove(key)
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
        
    def get_records_by_secondary_key(self,set,keyname, keyvalue):
        '''
        Fetches records from given set on the basis of secondary key
         and its value
         
        :param set: Name of the set from which record is to be fetched.
        :param keyname: secondary key of set.
        :param keyvalue: secondary key  value.
        :return list of records  if success  else aerospike exception
        '''
        try:
            query = client.query( NAMESPACE, set )
            query.where(p.equals(keyname,keyvalue))
            records = []
            # callback method to iterate over query result  
            def callback((key, metadata, record)):
                records.append(record)
            query.foreach(callback)
            return records
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
    
    def get_all_records_of_set(self,set):
        '''
        Fetches all the records from given set
         
        :param set: Name of the set from which records are to be fetched.
        :return list of records  if success  else aerospike exception 
        '''
        try:
            query = client.query( NAMESPACE, set )
            records = []
            def callback((key, metadata, record)):
                records.append(record)
            query.foreach(callback)
            return records
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
    
    def create_integer_type_index(self,set,bin,index_name):
        '''
        Creates integer type index on the basis of given set name , 
        bin name.
         
        :param set: Name of the set where index is to be created.
        :param bin: Name of the bin for which index is to be created.
        :param index_name: Index name.
        '''
        try:
            client.index_integer_create(NAMESPACE,set,bin,index_name)
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
            
        
    def create_string_type_index(self,set,bin,index_name):
        '''
        Creates string type index on the basis of given set name , 
        bin name.
         
        :param set: Name of the set where index is to be created.
        :param bin: Name of the bin for which index is to be created.
        :param index_name: Index name.
        '''
        try:
            client.index_string_create(NAMESPACE,set,bin,index_name)
        except AerospikeError as e:
            custom_logger(e.msg)
            return self.__handle_exception()
        
        
    def __handle_exception(self):
        return RestResponse(None, httplib.INTERNAL_SERVER_ERROR, 'Something went wrong', False).to_json()
        
    
        
    
        

        
