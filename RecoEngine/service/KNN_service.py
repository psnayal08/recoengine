# -*- coding: utf-8 -*-
import nltk
from nltk.tokenize.regexp import RegexpTokenizer
from schema.article import Article
from service.aerospike_db_manager import AeroSpikeDbManager
from constant.constants import ARTICLE_SET
from service.article_similarity_service import ArticleSimilarity
from operator import itemgetter
import operator
import copy
from collections import Counter

class KnnService:
    __aero_spikedb_manager = AeroSpikeDbManager()
    __article_similarity = ArticleSimilarity()
    
  
    def batchh_calculator_for_similar_article_with_knn(self,article):
        '''
            Runs a batch calculator whenever a new article is pushed to db 
            for determining most similar articles to given article by using knn
            :param article: article for which similar articles are to be calculated  .
       
        '''
        
        recommend_articles = []
        article[Article.SMPL_CLASSES] =self.__get_part_of_speech_tagging_with_NLTK(article[Article.TITLE])
        articles = self.__aero_spikedb_manager.get_all_records_of_set(ARTICLE_SET)
        if len(articles)< 1:
            article[Article.KNN_CLASS] = article[Article.SMPL_CLASSES][0]
            self.__aero_spikedb_manager.save_record(set, article[Article.ID], article)
        else:
            for existing_article in articles:
                if Article.ID in existing_article and  existing_article[Article.ID] != article[Article.ID]:
                    if Article.SMPL_CLASSES not in existing_article or len(existing_article[Article.SMPL_CLASSES] ) <1 :
                        existing_article = self.save_article_with_sample_classes(existing_article)
                    e_distance_weightage = str(-(self.__article_similarity.calculate_euclidean_distance_between_two_articles_by_keywords(article[Article.SMPL_CLASSES],existing_article[ Article.SMPL_CLASSES])))
                    if 'e'in e_distance_weightage or 'E'in e_distance_weightage:
                        e_distance_weightage = float(e_distance_weightage.split('e')[0])
                    else:
                        e_distance_weightage = float(e_distance_weightage)
                    recommend_articles.append({Article.WEIGHTAGE :e_distance_weightage ,Article.ID:existing_article[Article.ID],Article.SMPL_CLASSES:existing_article[Article.SMPL_CLASSES]})
                    self.__post_knn_calculation(recommend_articles,article)
    

    def save_article_with_sample_classes(self,article):
        '''
        fetches sample classes for an article by using  nltk pos tagging
        :param article: article for which get the sample classes i.e verb or noun .
        :return saved article
        
        '''
        article[Article.SMPL_CLASSES] = self.__get_part_of_speech_tagging_with_NLTK(article[Article.TITLE])
        return self.__aero_spikedb_manager.save_record(ARTICLE_SET, article[Article.ID], article)
    
    def __post_knn_calculation(self, recommend_articles, article):
        '''
        Sort the given articles according to weightge key in desc order
        and save into db.
         
        :param recommend_articles: list of  recommended article ids for given article .
        :param article: article for which similar articles are to be calculated  
        ''' 
        
        sample_classes = [] 
        final_recommended_article_list = []  
        sorted_recommended_article_list = sorted(recommend_articles, key=itemgetter(Article.WEIGHTAGE),reverse = True)
        if len(sorted_recommended_article_list)>= 36:
            sorted_recommended_article_list = sorted_recommended_article_list[0:36]
            
        for sorted_recommended_article in sorted_recommended_article_list:
            sample_classes  = sample_classes +  sorted_recommended_article[Article.SMPL_CLASSES] 
        
        sorted_dict = self.__calculate_occurance(article[Article.SMPL_CLASSES],sample_classes)
        sorted_classification_list = [ key for key, value in sorted_dict.iteritems()]
        article[Article.KNN_CLASS] = sorted_classification_list[0]
        
        for sorted_recommended_article in  sorted_recommended_article_list:
            if article[Article.KNN_CLASS]  in sorted_recommended_article[Article.SMPL_CLASSES]:
                final_recommended_article_list.append(sorted_recommended_article[Article.ID])
                if len(final_recommended_article_list) == 6:
                    break
        article[Article.SIMILAR_ARTICLES] = final_recommended_article_list
        self.__aero_spikedb_manager.save_record(ARTICLE_SET, article[Article.ID], article)
        
    def __get_part_of_speech_tagging_with_NLTK(self,text):
        '''
        Calculates sample tags using  nltk pos tagging
        :param text: article for which get the sample classes i.e verb or noun  .
        :return list of POS tags
        '''
        cloned_text = copy.deepcopy(text)
        stop_words  = []
        words = []
        with open("../SmartStoplist.txt",'r') as f:
            for line in f:
                for word in line.split():
                    stop_words.append(word)

        tokenizer = RegexpTokenizer(r'\w+')
        cloned_text = tokenizer.tokenize(cloned_text)
        filtered_text = [w for w in cloned_text if not w.lower() in stop_words]
        nltk_list= nltk.pos_tag(filtered_text)
        verbs = ["VB" "VBD" "VBG" "VBN" "VBP" "VBZ"]
        nouns = ["NNP","NNPS"]
        
        for i in range(len(nltk_list)):
            if nltk_list[i][1] in verbs:
                words.append(nltk_list[i][0])
                
        if len(words)<5 :
            for i in range(len(nltk_list)):
                if nltk_list[i][1] in nouns and len(words)<5:
                    words.append(nltk_list[i][0])
        return words
    
    
    def __calculate_occurance(self, article_sample_classes , sample_classes):
        classes_dict = {}
        sample_classes_counter = Counter(sample_classes)
        for article_sample_class in article_sample_classes:
            if article_sample_class in sample_classes_counter:
                classes_dict[article_sample_class] = sample_classes_counter[article_sample_class] 
            else:
                classes_dict[article_sample_class] = 0
        sorted(classes_dict.items(), key=operator.itemgetter(1,0), reverse=True)
        return classes_dict
                
                
    
           
            
            
