'''
Created on 22-Jan-2016

@author: prakash
'''
import tornado.ioloop
import tornado.web

def make_app():
    return tornado.web.Application([
                (r"/", 'controller.user_ctrl.UserCtrl'),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
    
