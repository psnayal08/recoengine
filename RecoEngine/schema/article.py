

import json
from schema import Schema, And, Use, Optional

class Article:
    ID = 'id'
    CATEGORIES = 'categories'
    TAGS = 'tags'
    SIMILAR_ARTICLES = 'smlr_articles'
    COSINE_DISTENCE =  'cosine_distence'
    WEIGHTAGE = ' weightage'
    TITLE = 'title'
    SMPL_CLASSES = 'smpl_classes'
    KNN_CLASS = 'knn_class'
    
    
    VALIDATOR = Schema(And(Use(json.loads), {
        ID: basestring,
        CATEGORIES: list,
        TAGS : list,
        TITLE: basestring,
        Optional(SIMILAR_ARTICLES) : [{WEIGHTAGE:object, ID: basestring}] ,
        Optional(SMPL_CLASSES):list,
        Optional(KNN_CLASS):list

    }))

