
import aerospike

# configure the client to first connect to a cluster node at 127.0.0.1
# the client will learn about the other nodes in the cluster from the
# seed node.
# in this configuration shared-memory cluster tending is turned on,
# which is appropriate for a multi-process context, such as a webserver
config = {
    'hosts':    [ ('127.0.0.1', 3000) ]
    }
client = aerospike.client(config).connect()

client.index_string_create('test','scw_article','category','category_index')
    
    