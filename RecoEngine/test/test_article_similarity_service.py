# -*- coding: utf-8 -*-
from service.article_similarity_service import ArticleSimilarity
from service.aerospike_db_manager import AeroSpikeDbManager
from constant.constants import ARTICLE_SET
from test.test_base import BaseTest
from schema.article import Article
from pandas.core.frame import DataFrame
from scipy.spatial.distance import euclidean
import RAKE
import nltk
from nltk.tokenize.regexp import RegexpTokenizer
from util.sample_scoop_whoop_data import SampleScoopWhoopData
from service.KNN_service import KnnService

class TestArticleSimilarity(BaseTest):
    __article_similarity_service = ArticleSimilarity()
    __areospike_db_manager = AeroSpikeDbManager()
    __sample_scoop_whoop_data = SampleScoopWhoopData()
    __KNN_service = KnnService()
    
    def test_if_find_records_by_caregory_working(self):
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        print articles[0]
        self.assertIsNotNone(articles)
    
            
    
    def test_if_get_similar_articles_working(self):
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        article = articles[0]
        articles = self.__article_similarity_service.get_similar_articles(article[Article.ID], article[Article.TITLE], article[Article.CATEGORIES], article[Article.TAGS])
        self.assertIsNotNone(articles)
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        print articles[0]

    def test_if_get_similar_articles_by_knn_working(self):
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        article = articles[3]
        articles = self.__KNN_service.batchh_calculator_for_similar_article_with_knn(article)
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        print articles[3]
     
        
        
    def delete_all_records_from_set(self):
        articles = self.__areospike_db_manager.get_all_records_of_set(ARTICLE_SET)
        for article in articles:
            if Article.ID in article:
                self.__areospike_db_manager.delete_key(ARTICLE_SET, article[Article.ID])
            
    def test_if_rake(self):
        text = "Heres What Happened When Donald Trump Picked A Twitter Fight With A Saudi Prince"
        text2 = "17 Unspoken Car Etiquette Every Passenger Should Know"
        rake = RAKE.Rake("../SmartStoplist.txt")
        sample1= rake.run(text)
        sample2 = rake.run(text2)
        print sample1[0][0]
        print sample2
 

    
    def test_eucledian_dist(self):
        tags1 = ['hockey',' cricket','india']
        tags2 = ['hockey']
        print self.__article_similarity_service.calculate_euclidean_distance_between_two_articles_by_keywords(tags1, tags2)
        
    def test_if_data_save_from_scw(self):
        self.__article_similarity_service.save_sample_data()
    
    def nltk_test(self):
        stop_words  = []
        with open("../SmartStoplist.txt",'r') as f:
            for line in f:
                for word in line.split():
                    stop_words.append(word)

        tokenizer = RegexpTokenizer(r'\w+')
        text = tokenizer.tokenize(" Understand IS These Bollywood Movie Summaries Only If You Spend A Lot Of Time On The Internet")
        filtered_text = [w for w in text if not w.lower() in stop_words]
        print nltk.pos_tag(filtered_text)
    
    def build_sample_data(self):
        self.__sample_scoop_whoop_data.save_sample_data()
        
        
        
    
        
        
        
            
        