import RAKE


class RakeHelper:
    
    @staticmethod
    def get_list_of_keywords_using_rake(self,text):
        keywords = []
        rake = RAKE.Rake("../SmartStoplist.txt")
        rake_keywords = rake.run(text)
        for rake_keyword in rake_keywords:
            keywords.append(rake_keyword[0])
        return keywords