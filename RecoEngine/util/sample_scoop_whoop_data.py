import requests
import json
from schema.article import Article
from constant.constants import ARTICLE_SET
from service.aerospike_db_manager import AeroSpikeDbManager

class SampleScoopWhoopData:
    __areospike_db_manager = AeroSpikeDbManager()
    
    def fetch_data_from_api(self,value):
        data_details = requests.get("http://www.scoopwhoop.com/api/v1/all/?offset="+str(value))
        data= json.loads(data_details.text)
        return data['data']
    
    def save_sample_data(self):
        for i in range(10):
            data_list = self.fetch_data_from_api(i)
            for data in data_list:
                record = {}
                record[Article.ID] = str(data['_id'])
                record[Article.CATEGORIES] = data['category']
                record[Article.TITLE] = data['title']
                record[Article.TAGS] = data['tags']
            self.__areospike_db_manager.save_record(ARTICLE_SET, record[Article.ID], record)
            i=i+10
     
        
    
    