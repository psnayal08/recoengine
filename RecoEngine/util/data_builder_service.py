'''
Created on 23-Jan-2016

@author: prakash
'''
from service.aerospike_db_manager import AeroSpikeDbManager
from schema.article import Article
from constant.constants import ARTICLE_SET

class DataBuilderService:
    __aerospike_db_manager = AeroSpikeDbManager()
    
    def build_data(self):
        article1 = {
                'id':'1',
                'categories':['SPORTS'],
                'tags':['cricket','india','test_match'],
                'title':'Ravindra Jadeja Sues Rajkot Newspaper AabTak For Rs 51 Crores '
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article1[Article.ID], article1)
        
        article2 = {
                'id':'2',
                'categories':['SPORTS'],
                'tags':['cricket','AUS','T20'],
                'title':'Buoyant India cruise to series win '
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article2[Article.ID], article2)
        
        article3 = {
                'id':'3',
                'categories':['SPORTS'],
                'tags':['ENG','cricket','one_day'],
                'title':'South Africa v England Jason Roy on no-fear cricket ahead of ODIs'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article3[Article.ID], article3)
        
        article4 = {
                'id':'4',
                'categories':['SPORTS'],
                'tags':['France','football','UEFA Europa League'],
                'title':'Football Fifa presidential aspirant Champagne by France federation chief'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article4[Article.ID], article4)
        
        article5 = {
                'id':'5',
                'categories':['SPORTS'],
                'tags':['football','spain','FC Barcelona'],
                'title':'Ex Chelsea and France star Malouda joins Wadi Degla'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article5[Article.ID], article5)
        
        article6 = {
                'id':'6',
                'categories':['SPORTS'],
                'tags':['cricket','ENG','football'],
                'title':'Jos Buttler to keep wicket for England against South Africa A'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article6[Article.ID], article6)
        
        article7 = {
                'id':'7',
                'categories':['SPORTS'],
                'tags':['cricket','table tennis','racket'],
                'title':'Table tennis club to launch in East Village'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article7[Article.ID], article7)
        
        article8 = {
                'id':'8',
                'categories':['SPORTS'],
                'tags':['table_tennis','football','test_match'],
                'title':'New Zealand take on Pakistan in the second One Day International ODI at Napier on Thursday '
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article8[Article.ID], article8)
        
        article9 = {
                'id':'9',
                'categories':['SPORTS'],
                'tags':['AUS','hockey','Ice hockey'],
                'title':'Hockey  Australia and Germany play out 1 1 draw in TPG International Tri Series opener'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article9[Article.ID], article9)
        
        article10 = {
                'id':'10',
                'categories':['SPORTS'],
                'tags':['hockey',' Coal India Hockey','india'],
                'title':'While We Wait For Our Cricketers To Defend The World Cup Our Women Hockey Team Has Done Us Proud'
                }
        self.__aerospike_db_manager.save_record(ARTICLE_SET, article10[Article.ID], article10)
