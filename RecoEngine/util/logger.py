from datetime import datetime
import inspect
import logging
from constant.constants import LOGGER_PATH


def custom_logger(error):  
    logger = logging.getLogger()
    logger.setLevel(logging.ERROR)
    logger_date = datetime.now().strftime('%d%B%Y')
    handler = logging.FileHandler(LOGGER_PATH + '.' + logger_date + '.' + 'log')
    #handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)

    formatter = logging.Formatter('Message_Type : %(levelname)s Time:%(asctime)s   Message:%(message)s')
    handler.setFormatter(formatter)
    func = inspect.currentframe().f_back.f_code
    logger.addHandler(handler)
    if type(error) is list:
        file_name,line_no,function_name=error[-3].split(',')
        logger.error("%s: Function_Name : %s  Location : %s  error_lineNo : %s" % (
                error[-1], 
                function_name[4:], 
                file_name[7:], 
                line_no
                ))
    
    else:
        logger.error("%s: Function_Name : %s  Location : %s : %i  error_lineNo : %i" % (
            error, 
            func.co_name, 
            func.co_filename, 
            func.co_firstlineno,
            inspect.currentframe().f_back.f_lineno        
            ))